const fs = require('fs');
const path = require("path");
console.log('DHH\n\n');


//var destinationDir = '.\\ck3bookmarkmapdump\\output\\';
var destinationDir = '.\\BookmarkMapping\\';
var gameDir = 'D:\\SteamLibrary\\steamapps\\common\\Crusader\ Kings\ III\\game\\';
const localizationPath = 'localization\\english\\';
// const localizationFilename = 'titles_l_english.yml';
const localizationFilenames = [
	'titles_cultural_names_l_english.yml',
	'titles_l_english.yml',
];
var titlesPath = 'common\\landed_titles\\';
var titlesFile = '00_landed_titles.txt';

try {
	if (!fs.existsSync(path.join(destinationDir, localizationPath))){
			fs.mkdirSync(path.join(destinationDir, localizationPath), { recursive: true });
	}
	if (!fs.existsSync(path.join(destinationDir, titlesPath))){
		fs.mkdirSync(path.join(destinationDir, titlesPath), { recursive: true });
	}
} catch (err) {
  console.error(`Initialize Error: ${err}}`);
}

let content;

try {
	content = fs.readFileSync(path.join(gameDir, titlesPath, titlesFile)).toString();
	content = content.replace(/(color = {.*?})/g, 'color = { 255 249 198 }');
	content = content.replace(/(color =  hsv{.*?})/g, 'color = { 255 249 198 }');
	content = content.replace(/(color = hsv{.*?})/g, 'color = { 255 249 198 }');
	const titleMatches = content.match(/([e,k,d]_.*? = {)/g);
	titleMatches.forEach(titleMatch => {
		const titleMatchIndex = content.indexOf(titleMatch) + titleMatch.length;
		content = [content.slice(0, titleMatchIndex), `\ncan_be_named_after_dynasty = no\n`, content.slice(titleMatchIndex)].join('');
		console.log(`Match: ${titleMatch} ${titleMatchIndex} - ${content.length}`);
	});
	console.log('Transform file successfully\n\n');
} catch (err) {
  console.error(`Transform Error: ${err}}`);
}

try {
	fs.writeFile(path.join(destinationDir, titlesPath, titlesFile), content, err => {
		if (err) throw err;
		// file written successfully
		console.log(`Wrote file successfully: ${path.join(destinationDir, titlesFile)}\n\n`);
	});
} catch (err) {
  console.error(`Write Error: ${err}}`);
}

localizationFilenames.forEach(localizationFilename => {
	try {
		content = fs.readFileSync(path.join(gameDir, localizationPath, localizationFilename)).toString();
		content = content.replace(/(".*?")/g, '""');
		console.log('Transform Localization successfully\n\n');
	} catch (err) {
		console.error(`Transform Localization: ${err}}`);
	}
	
	try {
		fs.writeFile(path.join(destinationDir, localizationPath, localizationFilename), content, err => {
			if (err) throw err;
			// file written successfully
			console.log(`Wrote file successfully: ${path.join(destinationDir, localizationFilename)}\n\n`);
		});
	} catch (err) {
		console.error(`Write Error: ${err}}`);
	}
});
